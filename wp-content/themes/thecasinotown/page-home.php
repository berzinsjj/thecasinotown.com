<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<div class="page-container">
    <div class="page-content">
        <div class="paragraph">
            <div class="paragraph-text">
                <?php $first_paragraph = get_field('first_paragraph'); ?>
                <?php foreach ($first_paragraph as $text) : ?>
                <p><?php echo nl2br($text['text']); ?></p>
                <p class="read-more-text"><?php echo nl2br($text['text_2']); ?></p>
            </div>
            <div class="paragraph-read">
                <a class="read-more-link" href="/">
                    <span>Read More</span>
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" class="svg-inline--fa fa-chevron-down fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path></svg>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="first-bonus">
            <?php $second_paragraph = get_field('second_paragraph'); ?>
            <?php foreach ($second_paragraph as $text) : ?>
            <h1><?php echo $text['title']; ?></h1>
        </div>
        <div class="paragraph">
            <div class="paragraph-text">
                <p><?php echo nl2br($text['text']); ?></p>
                <p class="read-more-text"><?php echo nl2br($text['text_2']); ?></p>
            </div>
            <div class="paragraph-read">
                <a class="read-more-link" href="/">
                    <span>Read More</span>
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-down" class="svg-inline--fa fa-chevron-down fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path></svg>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="casino-container">
            <?php $featured_casinos = get_field('featured_casinos'); ?>
            <?php foreach ($featured_casinos as $company) : ?>
            <div class="oj-oj-oj">
            <div class="casino-box">
                <div class="casino-logo">
                    <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $company->ID); ?>"></a>
                </div>
                <div class="casino-name">
                    <h3><?php echo $company->post_title; ?></h3>
                    <p><?php echo get_field('welcome_bonus', $company->ID); ?></p>
                </div>
                <div class="casino-bonus">
                    <h3><?php echo get_field('money_bonus', $company->ID); ?></h3>
                    <?php if (!empty(get_field('spin_bonus', $company->ID))) : ?>
                        <p><?php echo get_field('spin_bonus', $company->ID); ?></p>
                    <?php endif; ?>
                </div>
                <div class="casino-paragraph">
                    <p><?php echo get_field('casino_text', $company->ID); ?></p>
                </div>
                <div class="casino-button-terms">
                    <div class="casino-button">
                        <a href="<?php echo get_field('affialite_link', $company->ID); ?>" target="_blank">Visit Casino</a>
                    </div>
                    <div class="casino-terms">
                        <a href="<?php echo get_field('terms_link', $company->ID); ?>" target="_blank"><?php echo get_field('terms_text', $company->ID); ?></a>
                    </div>
                </div>
            </div>
                <?php if (!empty(get_field('terms_info', $company->ID))) : ?>
            <div class="casino-terms-text">
                <p><?php echo get_field('terms_info', $company->ID); ?></p>
            </div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="casino-bonus-container">
            <?php $first_casino_bonus = get_field('first_casino_bonus'); ?>
            <?php foreach ($first_casino_bonus as $text) : ?>
                <div class="casino-bonus-content">
                    <h3><?php echo $text['title']; ?></h3>
                    <div class="casino-bonus-box">
                        <div class="casino-bonus-box-logo">
                            <img src="<?php echo $text['logo']; ?>">
                        </div>
                        <div class="casino-bonus-box-text">
                            <?php echo $text['text']; ?>
                        </div>
                        <div class="casino-bonus-box-text2">
                            <?php echo $text['text_2']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="paragraph2">
            <?php $third_paragraph = get_field('third_paragraph'); ?>
            <?php foreach ($third_paragraph as $text) : ?>
            <h3><?php echo $text['title']; ?></h3>
            <div class="paragraph2-text">
                <?php echo nl2br($text['text']); ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="casino-bonus-container">
            <?php $second_casino_bonus = get_field('second_casino_bonus'); ?>
            <?php foreach ($second_casino_bonus as $text) : ?>
                <div class="casino-bonus-content">
                    <h3><?php echo $text['title']; ?></h3>
                    <div class="casino-bonus-box">
                        <div class="casino-bonus-box-logo">
                            <img src="<?php echo $text['logo']; ?>">
                        </div>
                        <div class="casino-bonus-box-text">
                            <?php echo $text['text']; ?>
                        </div>
                        <div class="casino-bonus-box-text2">
                            <?php echo $text['text_2']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="casino-guides-container">
            <?php $guides_block_title = get_field('guides_block_title'); ?>
            <h3><?php echo $guides_block_title; ?></h3>
            <div class="casino-guides-content">
                <?php $most_popular_guides = get_field('guides'); ?>
                <?php foreach ($most_popular_guides as $posts) : ?>
                <div class="casino-guides-box">
                    <div class="casino-guides-text">
                        <h4><?php echo $posts->post_title; ?></h4>
                    </div>
                    <div class="casino-guides-button">
                        <a href="<?php echo get_permalink($posts); ?>">Read More</a>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="paragraph2">
            <?php $fourth_paragraph = get_field('fourth_paragraph'); ?>
            <?php foreach ($fourth_paragraph as $text) : ?>
            <h3><?php echo $text['title']; ?></h3>
            <div class="paragraph2-text">
                <?php echo nl2br($text['text']); ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="sign-up">
        <div class="sign-up-you">
            <?php $sign_up = get_field('sign_up'); ?>
            <h3><?php echo $sign_up['title']; ?></h3>
            <h4><?php echo $sign_up['text']; ?></h4>
        </div>
        <form method="post" action="#">
        <div class="sign-up-email">
            <input type="text" class="email-input" name="email" placeholder="Your e-mail....." />
            <button class="blinker">Sign Up</button>
        </div>
        </form>
        <div class="notifications"></div>
        <div class="sign-up-paragraph">
            <p><?php echo $sign_up['text_2']; ?></p>
        </div>
    </div>
</div>
<?php get_footer(); ?>
