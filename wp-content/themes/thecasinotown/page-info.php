<?php /* Template Name: Info */ ?>
<?php get_header(); ?>
    <div class="page-container">
        <div class="page-content">
            <div class="page-info">
                <div class="page-info-text">
                    <?php $page = get_post(get_the_ID()); ?>
                    <h1><?php echo $page->post_title; ?></h1>
                </div>
                <div class="page-info-paragraph">
                    <?php echo nl2br($page->post_content); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>