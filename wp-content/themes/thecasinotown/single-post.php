<?php get_header(); ?>
<?php $page = get_post(get_the_ID()); ?>
    <div class="page-container">
        <div class="page-content">
            <div class="single-post">
                <div class="single-post-text">
                    <h1><?php echo $page->post_title; ?></h1>
                </div>
                <div class="single-post-paragraph">
                    <?php echo nl2br($page->post_content); ?>
                </div>
                <div class="casino-guides-container">
                    <?php $guides_block_title = get_field('guides_block_title' ,8); ?>
                    <h3><?php echo $guides_block_title; ?></h3>
                    <div class="casino-guides-content">
                        <?php $most_popular_guides = get_field('most_popular_guides'); ?>
                        <?php if ($most_popular_guides !== null) : ?>
                            <?php foreach ($most_popular_guides as $posts) : ?>
                                <div class="casino-guides-box">
                                    <div class="casino-guides-text">
                                        <h4><?php echo $posts->post_title; ?></h4>
                                    </div>
                                    <div class="casino-guides-button">
                                        <a href="<?php echo get_permalink($posts); ?>">Read More</a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="sign-up">
            <div class="sign-up-you">
                <?php $sign_up = get_field('sign_up', 8); ?>
                <h3><?php echo $sign_up['title']; ?></h3>
                <h4><?php echo $sign_up['text']; ?></h4>
            </div>
            <form method="post" action="#">
                <div class="sign-up-email">
                    <input type="text" class="email-input" name="email" placeholder="Your e-mail....." />
                    <button class="blinker">Sign Up</button>
                </div>
            </form>
            <div class="notifications"></div>
            <div class="sign-up-paragraph">
                <p><?php echo $sign_up['text_2']; ?></p>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
