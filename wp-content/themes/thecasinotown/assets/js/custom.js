jQuery(function ($) {
    jQuery(document).ready(function () {
        // Mobile menu
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-main-menu-container").toggleClass("is-active");
        });

        // Contact form
        $(".blinker").on("click", function(e) {
            e.preventDefault();
            var email_address = $(".sign-up input[name='email']").val();
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            if(pattern.test(email_address) != true) {
                $(".notifications").html("Please enter valid e-mail!");
            } else if(email_address == "") {
                $(".notifications").html("Please enter your e-mail!");
            } else {
                $(".notifications").html("Thank you, e-mail has been submitted!!");
                $(".sign-up input[name='email']").val("");
            }
        });

        //Read More Poga
        $(".read-more-link").on("click", function(e) {
            e.preventDefault();
            var hidden_text = $(this).parent().parent().find(".read-more-text");
            var read_more_button = $(this);
            hidden_text.toggle();
            if (hidden_text.css("display") === "block") {
                read_more_button.find("span").html("Read Less");
                read_more_button.find("svg").css("transform", "rotate(180deg)");
            } else if (hidden_text.css("display") === "none") {
                read_more_button.find("span").html("Read More");
                read_more_button.find("svg").css("transform", "rotate(0)");
            }

        });

    });
});