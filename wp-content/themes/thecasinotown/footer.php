<footer>
    <?php $footer = get_field('footer', 8); ?>
    <div class="footer-menu-background">
        <div class="footer-menus">
            <div class="footer-menu">
                <?php echo wp_nav_menu(array('menu' => 'Footer menu')); ?>
            </div>
            <div class="footer-icons">
                <?php foreach ($footer['footer_icon'] as $icon) : ?>
                    <div class="footer-icon">
                        <a href="<?php echo $icon['svg_link']['url']; ?>" target="_blank">
                            <?php echo $icon['svg']; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="footer-menu-2">
                <?php echo wp_nav_menu(array('menu' => 'Footer menu 2')); ?>
            </div>
            <div class="copyright">
                <p><?php echo $footer['copyright']; ?></p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>